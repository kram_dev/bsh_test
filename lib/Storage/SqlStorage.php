<?php

namespace Storage;

use PDO;
use PDOException;

class SqlStorage implements StorageInterface
{

    /** @var \PDO */
    private $DBH;

    public function __construct($host, $dbname, $user, $pass)
    {
        try {
            $this->DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param array $params
     *
     * @return boolean
     */
    public function save($params)
    {
        $STH = $this->DBH->prepare("INSERT INTO result (srcId, email) values (:srcId, :email)");

        return $STH->execute($params);
    }

    /**
     * @param string $email
     * @param int $status
     *
     * @return boolean
     */
    public function deliveryStatus($email, $status)
    {
        $STH = $this->DBH->prepare("UPDATE result set delivery_status = :delivery_status WHERE email = :email");

        return $STH->execute(['email' => $email, 'delivery_status' => $status]);
    }
}