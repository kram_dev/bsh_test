<?php

namespace Storage;

interface StorageInterface {

    /**
     * @param array $params
     *
     * @return boolean
     */
    public function save($params);

    /**
     * @param string $email
     * @param int $status
     *
     * @return boolean
     */
    public function deliveryStatus($email, $status);
}
