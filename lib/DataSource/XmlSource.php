<?php
namespace DataSource;

use DOMDocument;
use Dto\RecipientDto;
use XMLReader;

class XmlSource implements DataSourceInterface
{
    private $xmlReader;
    private $xmlDoc;

    public function __construct($sourceFile)
    {
        //читаем через потоковый ридер, на случай если файл будет длинным
        $this->xmlReader = new XMLReader();
        if (!$this->xmlReader->open($sourceFile)) {
            die("Failed to open '{$sourceFile}'");
        }

        $this->xmlDoc = new DOMDocument();

        while ($this->xmlReader->read() && $this->xmlReader->name !== 'email') ;
    }

    public function __destruct()
    {
        unset($this->xmlDoc, $this->xmlReader);
    }

    /**
     * @return RecipientDto|bool
     */
    public function getNext()
    {
        if ($this->xmlReader->name === 'email') {

            $node = simplexml_import_dom($this->xmlDoc->importNode($this->xmlReader->expand(), true));
            $nodeArr = $this->object2array($node);

            $recipientDtp = new RecipientDto();
            $recipientDtp->setId((int)$nodeArr['@attributes']['id']);
            $recipientDtp->setTo($nodeArr['to']);
            $recipientDtp->setSubject($nodeArr['subject']);

            $this->xmlReader->next('email');

            return $recipientDtp;
        } else {
            return false;
        }

    }

    private function object2array($object)
    {
        return json_decode(json_encode($object), true);
    }
}
