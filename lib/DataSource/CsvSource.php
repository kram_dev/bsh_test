<?php

namespace DataSource;

use Dto\RecipientDto;

class CsvSource implements DataSourceInterface
{
    private $filed;

    public function __construct($sourceFile)
    {
        $this->filed = fopen($sourceFile, 'r');;
    }

    public function __destruct()
    {
        fclose($this->filed);
    }

    /**
     * @return RecipientDto|bool
     */
    public function getNext()
    {
        if (!feof($this->filed)) {
            $line = fgetcsv($this->filed);

            $recipient = new RecipientDto();
            $recipient->setId($line[0]);
            $recipient->setTo($line[1]);
            $recipient->setSubject($line[2]);

            return $recipient;
        } else {
            return false;
        }
    }
}
