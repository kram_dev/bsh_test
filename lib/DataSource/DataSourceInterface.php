<?php
namespace DataSource;

use Dto\RecipientDto;

interface DataSourceInterface
{
    /**
     * @return RecipientDto
     */
    public function getNext();
}