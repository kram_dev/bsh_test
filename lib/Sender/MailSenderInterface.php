<?php
namespace Sender;

use Dto\RecipientDto;

interface MailSenderInterface
{
    /**
     * @param RecipientDto $recipientDto
     * @param string $body
     *
     * @return int
     */
    public function send(RecipientDto $recipientDto, $body);
}