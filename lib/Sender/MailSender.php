<?php

namespace Sender;

use Dto\RecipientDto;
use Sender\Transport\TransportInterface;

class MailSender implements MailSenderInterface
{
    private $transport;

    public function __construct(TransportInterface $transport)
    {
        $this->transport = $transport;
    }

    /**
     * @param RecipientDto $recipientDto
     * @param string $body
     *
     * @return int
     */
    public function send(RecipientDto $recipientDto, $body)
    {
        return $this->transport->send($recipientDto->getTo(), $recipientDto->getSubject(), $body);
    }
}