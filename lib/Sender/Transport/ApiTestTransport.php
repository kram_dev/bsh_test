<?php

namespace Sender\Transport;

/**
 * Class ApiTestTransport
 *
 * @package     Sender\Transport
 *              параметры запроса:
 *              id: формат значения целое число
 *              message формат значения JSON {"to":"...","subject":"..."}
 *              ответы API (HTTP status code):
 *              200 - письмо было успешно обработано, статус доставки будет отражен в теле ответа
 *              0 - письмо не доставлено
 *              1 - письмо доставлено
 *              503 -  сервис занят, повторить запрос позднее
 *              400 - некорректный запрос (ошибка будет отражена в теле ответа)
 */
class ApiTestTransport implements TransportInterface
{
    private $url;
    private $method;

    public function __construct($url = "", $method = 'POST')
    {
        $this->url    = $url;
        $this->method = $method;
    }


    public function send($to, $subject, $body)
    {
        $retrain = true;

        do {
            $resultRaw = $this->request(json_encode(['to' => $to, 'subject' => $subject]));

            if ($resultRaw) {
                $result = $this->parseResponse($resultRaw);
                switch ($result[0]) { //code
                    case '200':
                        $retrain = false;
                        $result  = $result[2]; //body
                        break;
                    case '400':
                        $retrain = false;
                        echo $result[2];
                        break;
                    case '503':
                        $retrain = true;
                        sleep(1);
                        break;
                }
            } else {
                return false;
            }
        } while ($retrain);

        return $result;
    }

    private function request($jsonData)
    {
        $ch = curl_init($this->url);
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_POST           => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS     => [
                    'id'      => mt_rand(1111, 9999),
                    'message' => $jsonData,
                ]
            )
        );

        $response = curl_exec($ch);

        return $response;
    }

    /**
     * @param string $response
     *
     * @return array
     */
    private function parseResponse($response)
    {
        /*
            result:
            [0] => the HTTP error or response code such as 404
            [1] => Array (headers)
            [2] => Response body (string)
        */

        //просто вырезаем лишний заголовок который предположительно вставляет прокси
        $response = str_replace("Connection established\r\n\r\n", "", $response);

        list($response_headers, $response_body) = explode("\r\n\r\n", $response, 2);
        $response_header_lines = explode("\r\n", $response_headers);

        // first line of headers is the HTTP response code
        $http_response_line = array_shift($response_header_lines);
        if (preg_match(
            '@^HTTP/[0-9]\.[0-9] ([0-9]{3})@',
            $http_response_line,
            $matches
        )
        ) {
            $response_code = $matches[1];
        }

        // put the rest of the headers in an array
        $response_header_array = array();
        foreach ($response_header_lines as $header_line) {
            list($header, $value) = explode(': ', $header_line, 2);
            $response_header_array[$header] = $value;
        }

        return [$response_code, $response_header_array, $response_body];
    }

}
