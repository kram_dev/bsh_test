<?php

namespace Sender\Transport;


class SmtpTransport implements TransportInterface
{
    private $host;
    private $port;
    private $mailFrom;
    private $password;

    public function __construct(
        $host,
        $port,
        $mailFrom,
        $password = null
    )
    {
        $this->host     = $host;
        $this->port     = $port;
        $this->mailFrom = $mailFrom;
        $this->password = $password;
    }

    public function send($to, $subject, $body)
    {
        // тут мог быть smtp транспорт =)
    }
}