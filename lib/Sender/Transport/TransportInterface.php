<?php

namespace Sender\Transport;

interface TransportInterface
{
    public function send($to, $subject, $body);
}