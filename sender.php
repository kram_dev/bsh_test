<?php
use DataSource\XmlSource;
use Sender\MailSender;
use Sender\Transport\ApiTestTransport;

include_once('vendor/autoload.php');

$dataSource = new XmlSource($argv['1']);

$apiSenderTransport = new ApiTestTransport('http://test.webjet.pro/api.php');
$sender             = new MailSender($apiSenderTransport);

$storage = new \Storage\SqlStorage('localhost', 'bsh_sender', 'bsh', 'qQ12#');

$body = 'Тестовая рассылка';

while (false != ($recipientDto = $dataSource->getNext())) {

    $params = [
        'srcId' => $recipientDto->getId(),
        'email' => $recipientDto->getTo(),
    ];

    if ($storage->save($params)) {

        $res = $sender->send($recipientDto, $body);
        $storage->deliveryStatus($recipientDto->getTo(), $res);
    }
}

